package org.academiadecodigo.bootcamp56.closestneighbours;

import java.util.Arrays;

public class closestNeighbours {

    public static void main(String[] args) {

        int[] myArray = {0, 5, 1209, 6, 2, 111, 112, 33};
        int[] result = findClosest(myArray);

        System.out.println(Arrays.toString(result));
    }

    private static int[] findClosest(int[] numbers) {

        int[] pair = new int[2];

        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < numbers.length; j++) {

                int difference = Math.abs(numbers[i] - numbers[j]);
                if (difference == 1) {
                    pair[0] = numbers[j];
                    pair[1] = numbers[i];
                }
            }
        }
        return pair;
    }
}
