class FrontBack {

	public static void main(String[] args) {

		frontBack("heisenberg");
		
		if (args.length > 0) {
			frontBack(args[0]);
		}
	}

	private static void frontBack(String str) {
		
		char firstChar = str.charAt(0);
		char lastChar = str.charAt(str.length() - 1);
		String middleValue = str.substring(1, str.length() - 1);

		System.out.println(lastChar + middleValue + firstChar);
	}
}
