package org.academiadecodigo.bootcamp56.deliverycenter;

public class DeliveryCenter {

    public static void main(String[] args) {

        Order pantufas = new Order(
                "Hipercentro, fração N",
                "Kidzania");
        Order raspeberryPi = new Order(
                "Hipercentro, fração N",
                "pcdiga");

        System.out.println("pantufas id " + pantufas.getId());
        System.out.println("raspeberryPi id " + raspeberryPi.getId());
    }
}
