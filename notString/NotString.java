class NotString {

	public static void main(String[] args) {
		
		notString("semicolon");
		notString("not semicolon");

		if (args.length > 0) {
			notString(args[0]);
		}
	}

	private static void notString(String str) {
		
		
		String not = str.substring(0, 3);

		if (not.equals("not")) {
			System.out.println(str);
		} else {
			System.out.println("not " + str);
		}
	}
}
