class Manipulations {

	public static void main(String[] args) {

		manipulations("http://www.academiadecodigo.org");

		if (args.length > 0) {
			manipulations(args[0]);
		}
	}

	private static void manipulations(String str) {
		
		String domain = str.substring(str.indexOf("w"));
		String name = str.substring(11, 12).toUpperCase() + str.substring(12, 19) + " " + str.substring(19, 21) + " " + str.substring(21, 22).toUpperCase() + str.substring(22, 27);

		System.out.println("I am a Code Cadet at < " + name + "_ >, " + domain);
	}
}
		

