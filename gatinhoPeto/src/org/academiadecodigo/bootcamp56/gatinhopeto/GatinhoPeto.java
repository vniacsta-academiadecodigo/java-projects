package org.academiadecodigo.bootcamp56.gatinhopeto;

import java.awt.desktop.SystemEventListener;

public class GatinhoPeto {

    private String name;
    private String color;
    private String eyeColor;
    private int age;
    private boolean cutie;
    private boolean hungry;

    public GatinhoPeto(String name, String color, String eyeColor, int age, boolean cutie, boolean hungry) {
        this.name = name;
        this.color = color;
        this.eyeColor = eyeColor;
        this.age = age;
        this.cutie = cutie;
        this.hungry = hungry;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public int getAge() {
        return age;
    }

    public boolean isCutie() {
        return cutie;
    }

    public boolean isHungry() {
        return hungry;
    }

    public void setCutie(boolean cutie) {
        if (cutie) {
            System.out.println("I am the cutest kitten in the whole wide world");
        }
        this.cutie = cutie;
    }

    public void setHungry(boolean hungry) {
        if (hungry) {
            System.out.println("I am always so hungry");
        }
        this.hungry = hungry;
    }

    public void playing(String[] toys) {
        for (String toy : toys) {
            System.out.println("I love playing with " + toy);
        }
    }
}
