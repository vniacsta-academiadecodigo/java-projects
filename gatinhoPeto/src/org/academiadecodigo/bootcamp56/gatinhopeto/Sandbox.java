package org.academiadecodigo.bootcamp56.gatinhopeto;

public class Sandbox {

    public static void main(String[] args) {

        GatinhoPeto nala = new GatinhoPeto("nala", "white and black", "green",
                1, true, true);

        nala.setCutie(true);
        nala.setHungry(true);
        nala.playing(new String[]{"blanket", "plastic", "sofa"});

        System.out.println("Hi, my name is " + nala.getName() + ", my colors are " + nala.getColor() +
                ". My beautiful piercing eyes are " + nala.getEyeColor() + " and I am " + nala.getAge() +
                " year(s) old.");
    }

}
