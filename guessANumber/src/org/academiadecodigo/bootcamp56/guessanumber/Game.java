package org.academiadecodigo.bootcamp56.guessanumber;

public class Game {

    // create method to start the game
    public void start(int maxNum) {

        // create random
        int target = Random.generateRandom(maxNum);
        System.out.print("Computer target number is " + target + "\n");

        // create players
        Player player1 = new Player("Vando");
        Player player2 = new Player("Ricardo");

        // create a variable to exit the game
        // check if random number from player1 or player2 is equal to target
        boolean gameIsFinished = false;

        while (!gameIsFinished) {

            // ask for random and print the guesses
            // class properties
            int guessPlayer1 = player1.guessTarget(maxNum);
            System.out.println(player1.getName() + " guessed with number " +
                    guessPlayer1);

            int guessPlayer2 = player2.guessTarget(maxNum);
            System.out.println(player2.getName() + " guessed with number " +
                    guessPlayer2);

            if (target == guessPlayer1) {
                System.out.println("I'm " + player1.getName() + " and my " +
                        "guess is " + guessPlayer1 + ". I won!");
                gameIsFinished = true;
                // continue goes back to the while, if the while is false,
                // it won't continue
                continue;
            }

            if (target == guessPlayer2) {
                System.out.println("I'm " + player1.getName() + " and my " +
                        "guess is " + guessPlayer2 + ". I won!");
                gameIsFinished = true;
            }
        }
    }
}
