package org.academiadecodigo.bootcamp56.guessanumber;

public class Random {

    // create generateRandom method
    public static int generateRandom(int maxNum) {
        // Math.round not needed because int already says its an integer
        // it needs the () because otherwise it would calculate the Math random
        // with the (int)
        return (int) (Math.random() * (maxNum + 1));
    }
}
