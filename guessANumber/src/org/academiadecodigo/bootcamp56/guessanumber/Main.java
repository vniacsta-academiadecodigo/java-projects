package org.academiadecodigo.bootcamp56.guessanumber;

public class Main {

    public static void main(String[] args) {

        // start the game with the range of guesses 0-10
        Game game = new Game();
        game.start(10);
    }
}
