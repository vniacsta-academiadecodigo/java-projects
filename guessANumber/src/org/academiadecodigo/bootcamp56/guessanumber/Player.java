package org.academiadecodigo.bootcamp56.guessanumber;

public class Player {

    // create variable to store the player's name
    private String name;

    // create method for player constructor
    public Player(String name) {

        this.name = name;
    }

    // allow the name to be read in other classes
    public String getName() {

        return name;
    }

    // it's not needed to declare the property maxNum because it will be
    // passed to this method when called
    public int guessTarget(int maxNum) {

        return Random.generateRandom(maxNum);
    }
}
