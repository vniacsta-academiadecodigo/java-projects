package org.academiadecodigo.bootcamp56.calculator;

import java.text.DecimalFormat;

public class Sandbox {

    public static void main(String[] args) {

        // creating an object/instance
        Calculator citizen = new Calculator("Citizen", "grey");

        // creating variables to store the result of the methods
        int resultAdd = citizen.add(43, 31);
        int resultSubtract = citizen.subtract(43, 31);
        int resultMultiply = citizen.multiply(43, 31);
        double resultDivide = citizen.divide(43, 31);

        // to format double to two decimals
        DecimalFormat resultDivideFormat = new DecimalFormat("#.00");

        System.out.println(resultAdd);
        System.out.println(resultSubtract);
        System.out.println(resultMultiply);
        System.out.println(resultDivideFormat.format(resultDivide));
    }
}
